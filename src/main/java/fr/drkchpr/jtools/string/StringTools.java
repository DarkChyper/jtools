package fr.drkchpr.jtools.string;

import fr.drkchpr.jtools.CommonConstants;

/**
 * Tools to manipulate String
 */
public class StringTools {

    public static boolean isNullEmptyOrBlank(final String string){
        return (null == string || CommonConstants.EMPTY_STRING.equals(string.trim()));
    }

    public static boolean isNotNullEmptyOrBlank(final String string){
        return !isNullEmptyOrBlank(string);
    }
}


