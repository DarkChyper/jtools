package fr.drkchpr.jtools.jcrypt;

import fr.drkchpr.jtools.CommonConstants;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

/**
 * Salt and hash a password with PBKDF2WithHmacSHA1
 */
public class JCrypt {


    private byte[] salt;
    private byte[] hash;

    /**
     * Default constructor
     */
    public JCrypt() {
        this.salt = CommonConstants.EMPTY_STRING.getBytes();
        this.hash = CommonConstants.EMPTY_STRING.getBytes();
    }

    /**
     * Generate Salt and Hash of password
     *
     * @param saltLenght      lenght of the salt at least 1
     * @param keyHashStrength lenght of the key and the hash
     * @param password        the password to hash
     * @throws JCryptException throw exception if needed
     */
    public void generate(final int saltLenght, final KeyHashStrength keyHashStrength, final String password) throws JCryptException {
        if (saltLenght < 1) {
            throw new JCryptException(JCryptConstants.SALT_LENGHT_ERROR_MSG);
        }
        setSalt(saltLenght);
        setHash(keyHashStrength, password);
    }


    /**
     * Compare the clear password
     * with the salt + hash
     *
     * @param salt       the salt
     * @param hashToTest the password hashed
     * @param password   the password to compare
     * @return true if the hashes are equals, false otherwise
     * @throws JCryptException throw Exception if needed
     */
    public boolean compare(final byte[] salt, final byte[] hashToTest, final String password) throws JCryptException {
        this.salt = salt;
        final KeyHashStrength keyHashStrength = defineKeyHashStrength(hashToTest);
        setHash(keyHashStrength, password);
        return this.hash == hash;
    }

    /**
     * Define the keyHashStrength with the hash previously created by JCrypt
     *
     * @param hashToTest the hash
     * @return KeyHashStrength
     * @throws JCryptException if the has was not created by JCrypt
     */
    private KeyHashStrength defineKeyHashStrength(byte[] hashToTest) throws JCryptException {

        for (KeyHashStrength khs : KeyHashStrength.values()) {
            if (hashToTest.length == khs.getHashLength()) {
                return khs;
            }
        }
        throw new JCryptException("The hash was not created with JCrypt");
    }

    /**
     * Get the salt
     *
     * @return salt
     */
    public byte[] getSalt() {
        return salt;
    }

    /**
     * Define the salt
     *
     * @param saltLength the lenght of the salt
     */
    private void setSalt(final int saltLength) {
        final SecureRandom random = new SecureRandom();
        byte[] salt = new byte[saltLength];
        random.nextBytes(salt);
        this.salt = salt;
    }

    /**
     * @param keyHashStrength the lenght of the final hash
     * @param password        the password to hash
     * @throws JCryptException throw exception if needed
     */
    private void setHash(final KeyHashStrength keyHashStrength, final String password) throws JCryptException {
        if (null == password || CommonConstants.EMPTY_STRING.equals(password.trim())) {
            throw new JCryptException(JCryptConstants.PASSWORD_EMPTY_ERROR_MSG);
        }
        try {
            final SecretKeyFactory factory = SecretKeyFactory.getInstance(JCryptConstants.PBKDF2WithHmacSHA1);
            final PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), this.salt, 65536, keyHashStrength.getKeyLength());
            this.hash = factory.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new JCryptException(e);
        }

    }


    /**
     * Get the hash
     *
     * @return hash
     */
    public byte[] getHash() {
        return hash;
    }

}
