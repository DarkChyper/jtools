package fr.drkchpr.jtools.jcrypt;

public interface JCryptConstants {

    String PBKDF2WithHmacSHA1 = "PBKDF2WithHmacSHA1";

    String SALT_LENGHT_ERROR_MSG = "The length of the salt must be more than 0";
    String PASSWORD_EMPTY_ERROR_MSG = "The password to hash must be not null or not empty";
}
