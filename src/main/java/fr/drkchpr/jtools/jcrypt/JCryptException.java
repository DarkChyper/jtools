package fr.drkchpr.jtools.jcrypt;

public class JCryptException extends Exception {

    public JCryptException(final Throwable cause) {
        super(cause);
    }

    public JCryptException(final String message) {
        super(message);
    }
}
