package fr.drkchpr.jtools.jcrypt;

public enum KeyHashStrength {

    WORST(256,32),
    BAD(512, 64),
    LOW(1024, 128),
    GOOD(2048,256),
    BEST(4096,512),
    MAD(8192, 1024),
    PARANOID(16384, 2048);

    /**
     * The length of encoding key
     */
    private int keyLength;

    /**
     * The length of the hash after incoding
     */
    private int hashLength;

    /**
     * private constructor
     * @param keyLength he length of encoding key
     * @param hashLength The length of the hash after incoding
     */
    KeyHashStrength(final int keyLength, final int hashLength){
        this.keyLength = keyLength;
        this.hashLength = hashLength;
    }

    /**
     * Get the length of encoding key
     */
    public int getKeyLength() {
        return this.keyLength;
    }

    /**
     * Get the length of the hash after incoding
     */
    public int getHashLength() {
        return hashLength;
    }
}
