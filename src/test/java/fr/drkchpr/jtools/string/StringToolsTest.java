package fr.drkchpr.jtools.string;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringToolsTest {

    @Test
    public void isNullEmptyOrBlank_should_return_true(){
        Assert.assertTrue("Null should return true with isNullEmptyOrBlank",StringTools.isNullEmptyOrBlank(null));
        Assert.assertTrue("Empty String should return true with isNullEmptyOrBlank",StringTools.isNullEmptyOrBlank(new String()));
        Assert.assertTrue("Blank String should return true with isNullEmptyOrBlank",StringTools.isNullEmptyOrBlank("    "));
    }

    @Test
    public void isNullEmptyOrBlank_should_return_false(){
        Assert.assertFalse("\"  a\" should return false with isNullEmptyOrBlank",StringTools.isNullEmptyOrBlank("  a"));
        Assert.assertFalse("\"a\" should return false with isNullEmptyOrBlank",StringTools.isNullEmptyOrBlank(new String("a")));
        Assert.assertFalse("\"  a  \" should return false with isNullEmptyOrBlank",StringTools.isNullEmptyOrBlank("  a  "));
    }

    @Test
    public void isNotNullEmptyOrBlank_should_return_true(){
        Assert.assertTrue("\"  a\" should return true with isNullEmptyOrBlank",StringTools.isNotNullEmptyOrBlank("  a"));
        Assert.assertTrue("\"a\" should return true with isNullEmptyOrBlank",StringTools.isNotNullEmptyOrBlank(new String("a")));
        Assert.assertTrue("\"  a  \" should return  with isNullEmptyOrBlank",StringTools.isNotNullEmptyOrBlank("  a  "));
    }

    @Test
    public void isNotNullEmptyOrBlank_should_return_false(){
        Assert.assertFalse("Null should return false with isNullEmptyOrBlank",StringTools.isNotNullEmptyOrBlank(null));
        Assert.assertFalse("Empty String should return false with isNullEmptyOrBlank",StringTools.isNotNullEmptyOrBlank(new String()));
        Assert.assertFalse("Blank String should return false with isNullEmptyOrBlank",StringTools.isNotNullEmptyOrBlank("     "));
    }
}