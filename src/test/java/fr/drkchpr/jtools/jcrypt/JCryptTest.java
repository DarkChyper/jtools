package fr.drkchpr.jtools.jcrypt;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.fail;

public class JCryptTest {

    private static final KeyHashStrength KEY_HASH_STRENGTH = KeyHashStrength.WORST;
    private static final int SALT_LENGTH_OK = 1;
    private static final int SALT_LENGTH_KO = 0;
    private static final String PASSWORD = "pass";

    @Test
    public void generate_should_set_salt_and_hash() {
        final JCrypt jCrypt = new JCrypt();

        // salt lenght must be > 0
        try {
            jCrypt.generate(SALT_LENGTH_KO, KEY_HASH_STRENGTH, PASSWORD);
            fail("Generate should throwed a JCryptException");
        } catch (JCryptException e) {
            Assert.assertEquals(JCryptConstants.SALT_LENGHT_ERROR_MSG, e.getMessage());
        }

        // password could not be empty
        try {
            jCrypt.generate(SALT_LENGTH_OK, KEY_HASH_STRENGTH, "");
            fail("Generate should throwed a JCryptException");
        } catch (JCryptException e) {
            Assert.assertEquals(JCryptConstants.PASSWORD_EMPTY_ERROR_MSG, e.getMessage());
        }

        try {
            jCrypt.generate(SALT_LENGTH_OK, KEY_HASH_STRENGTH, PASSWORD);
            Assert.assertEquals("The lenght of the hash is not as expected", KEY_HASH_STRENGTH.getHashLength(), jCrypt.getHash().length);
            Assert.assertEquals("The lenght of the salt is not as expected", SALT_LENGTH_OK, jCrypt.getSalt().length);
        } catch (JCryptException e) {
            fail("Generate has throwed a JCryptException : " + e.getMessage());
        }
    }

    @Test
    public void generate_twice_should_give_different_salt_and_hash() {
        final JCrypt jCrypt = new JCrypt();
        final JCrypt jCrypt2 = new JCrypt();

        try {
            jCrypt.generate(SALT_LENGTH_OK, KEY_HASH_STRENGTH, PASSWORD);
            jCrypt2.generate(SALT_LENGTH_OK, KEY_HASH_STRENGTH, PASSWORD);
            Assert.assertNotEquals("the salts should be different", jCrypt.getSalt(), jCrypt2.getSalt());
            Assert.assertNotEquals("the hashs should be different", jCrypt.getHash(), jCrypt2.getHash());
        } catch (JCryptException e) {
            fail("Generate has throwed a JCryptException : " + e.getMessage());
        }
    }


    @Test
    public void compare_should_return_true() {
        final JCrypt jCrypt = new JCrypt();
        final JCrypt jCrypt2 = new JCrypt();
        try {
            jCrypt.generate(SALT_LENGTH_OK, KEY_HASH_STRENGTH, PASSWORD);
            Assert.assertTrue("", jCrypt2.compare(jCrypt.getSalt(), jCrypt.getHash(), PASSWORD));
        } catch (JCryptException e) {
            fail("A JCryptException was throwed : " + e.getMessage());
        }
    }

    @Test
    public void compare_should_return_false() {
        final JCrypt jCrypt = new JCrypt();
        final JCrypt jCrypt2 = new JCrypt();
        try {
            jCrypt.generate(SALT_LENGTH_OK, KEY_HASH_STRENGTH, PASSWORD);
            Assert.assertTrue("", jCrypt2.compare(jCrypt.getSalt(), new byte[KEY_HASH_STRENGTH.getHashLength()], PASSWORD));
        } catch (JCryptException e) {
            fail("A JCryptException was throwed : " + e.getMessage());
        }
    }

    @Test
    public void compare_should_throw_exception() {
        final JCrypt jCrypt = new JCrypt();
        final JCrypt jCrypt2 = new JCrypt();
        try {
            jCrypt.generate(SALT_LENGTH_OK, KEY_HASH_STRENGTH, PASSWORD);
            jCrypt2.compare(jCrypt.getSalt(), new byte[12], PASSWORD);
            fail("A JCryptException should be throwed");
        } catch (JCryptException e) {
            Assert.assertEquals("The hash was not created with JCrypt", e.getMessage());
        }
    }
}